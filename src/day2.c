/* ==================================================================================
   $File: $
   $Date: 04 December 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdlib.h>

#include "common.h"

s32 main(s32 argc, char **argv)
{
    char *input_data = read_entire_file("day2.input");

    s32 *initial_intcode = 0; // NOTE(marshel): Stretchy buf
    Iterator iter = {0, ',', input_data};
    for (char *line = advance(&iter);
         *line;
         line = advance(&iter))
    {
        s32 code = atoi(line);
        buf_push(initial_intcode, code);
    }

    b32 running = true;
    for (s32 noun = 0;
         noun < 99 && running;
         ++noun)
    {
        for (s32 verb = 0;
             verb < 99;
             ++verb)
        {
            s32 *intcode = buf_copy(initial_intcode);
            intcode[1] = noun;
            intcode[2] = verb;

            for (s32 code_pos = 0;
                 code_pos < buf_len(intcode);
                 code_pos += 4)
            {
                s32 optcode = intcode[code_pos];
                if (optcode == 99) break; // NOTE(marshel): We're done

                s32 input_pos_1 = intcode[code_pos + 1];
                s32 input_pos_2 = intcode[code_pos + 2];
                s32 result_pos = intcode[code_pos + 3];

                s32 input_1 = intcode[input_pos_1];
                s32 input_2 = intcode[input_pos_2];
                switch (optcode)
                {
                    case 1:
                    {
                        // NOTE(marshel): Add
                        intcode[result_pos] = input_1 + input_2;
                    } break;

                    case 2:
                    {
                        // NOTE(marshel): Multiply
                        intcode[result_pos] = input_1 * input_2;
                    } break;

                    default:
                    {
                        assert(!"Invalid path");
                    } break;
                }
            }

            if (intcode[0] == 19690720)
            {
                running = false;
                printf("Noun: %d - Verb: %d - Result %d\n", noun, verb, 100 * noun + verb);
                break;
            }
        }
    }

    return 0;
}
