/* ==================================================================================
   $File: day7.c $
   $Date: 07 April 2020 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include "common.h"
#include "intcode_vm.c"

typedef struct
{
    s32 s[5];
} PhaseSequence;

typedef struct
{
    PhaseSequence *sequences; // NOTE(marshel): Stretchy buf
    PhaseSequence current_sequence;
} Sequences;

// NOTE(marshel): Heap's Algorithm for generating all possible
// permutations of the phase sequencing of {0, 1, 2, 3, 4}.
// https://en.wikipedia.org/wiki/Heap%27s_algorithm
void generate_phase_sequences(s32 step, Sequences *sequences)
{
    PhaseSequence *current_sequence = &sequences->current_sequence;
    if (step == 1)
    {
        buf_push(sequences->sequences, *current_sequence);
    } else {
        generate_phase_sequences(step - 1, sequences);

        for (s32 i = 0;
             i < step - 1;
             ++i)
        {
            if (step & 1)
            {
                s32 t = current_sequence->s[i];
                current_sequence->s[i] = current_sequence->s[step - 1];
                current_sequence->s[step - 1] = t;
            } else {
                s32 t = current_sequence->s[0];
                current_sequence->s[0] = current_sequence->s[step - 1];
                current_sequence->s[step - 1] = t;
            }
            generate_phase_sequences(step - 1, sequences);
        }
    }
}

s32 main(s32 argc, char **argv)
{
    s32 *intcode = 0; // NOTE(marshel): Stretchy buf
    char *input_data = read_entire_file("day7.input");
    Iterator iter = {0, ',', input_data};
    for (char *line = advance(&iter);
         *line;
         line = advance(&iter))
    {
        s32 value = atoi(line);
        buf_push(intcode, value);
    }

    Sequences sequences = {0, {0, 1, 2, 3, 4}};
    generate_phase_sequences(5, &sequences);

    s32 max_input_signal = 0;
    PhaseSequence best_sequence = sequences.sequences[0];
    for (s32 phase_index = 0;
         phase_index < buf_len(sequences.sequences);
         ++phase_index)
    {
        s32 *phase_sequence = sequences.sequences[phase_index].s;
        s32 input_values[2] = {0, 0};
        s32 input_signal = 0;
        for (s32 amp = 0;
             amp < 5;
             ++amp)
        {
            input_values[0] = phase_sequence[amp];
            input_values[1] = input_signal;
            input_signal = run_code(intcode, input_values, 2);
        }

        if (input_signal > max_input_signal)
        {
            max_input_signal = input_signal;
            best_sequence = sequences.sequences[phase_index];
        }
    }

    printf("Input Signal: %d\n", max_input_signal);
    for (s32 i = 0;
         i < 5;
         ++i)
    {
        printf("%d ", best_sequence.s[i]);
    }
    putchar('\n');

    return 0;
}
