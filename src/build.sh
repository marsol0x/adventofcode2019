# ==================================================================================
# $File: build.sh $
# $Date: 02 December 2019 $
# $Revision: $
# $Creator: Marshel Helsper $
# $Notice: $
# ==================================================================================

_day=day7
mkdir -p ../build
gcc -O0 -g -o ../build/adventofcode ${_day}.c
