/* ==================================================================================
   $File: $
   $Date: 20 January 2020 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>

#include "common.h"

b32 is_valid_password(char *number, s32 num_digits)
{
    char prev_digit = number[0];
    b32 has_double = false;
    s32 num_repeated = 0;
    for (s32 digit_index = 1;
         digit_index < num_digits;
         ++digit_index)
    {
        char cur_digit = number[digit_index];
        if (prev_digit > cur_digit)
        {
            return false;
        }

        if (prev_digit == cur_digit)
        {
            ++num_repeated;
        } else {
            if (num_repeated == 1)
            {
                has_double = true;
            }
            num_repeated = 0;
        }

        prev_digit = cur_digit;
    }

    return has_double || num_repeated == 1;
}

s32 main(s32 argc, char **argv)
{
    char number[7];
    s32 start = 284639;
    s32 end = 748759;

    assert(is_valid_password("111111", 6) == false);
    assert(is_valid_password("223450", 6) == false);
    assert(is_valid_password("123789", 6) == false);
    assert(is_valid_password("112233", 6) == true);
    assert(is_valid_password("123444", 6) == false);
    assert(is_valid_password("111122", 6) == true);

    s32 num_passwords = 0;
    for (s32 n = start;
         n <= end;
         ++n)
    {
        s32 digits = sprintf(number, "%d", n);
        number[digits] = 0;

        if (is_valid_password(number, digits))
        {
            ++num_passwords;
        }
    }

    printf("Num Passwords: %d\n", num_passwords);

    return 0;
}
