/* ==================================================================================
   $File: $
   $Date: 06 December 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "common.h"

typedef struct StepPoint
{
    Vec2 p;
    s32 steps;
} StepPoint;

s32 get_step_point_index(StepPoint *step_points, Vec2 p)
{
    for (s32 sp_index = 0;
         sp_index < buf_len(step_points);
         ++sp_index)
    {
        StepPoint sp = step_points[sp_index];
        if (sp.p.x == p.x && sp.p.y == p.y)
        {
            return sp_index;
        }
    }

    return -1;
}

s32 main(s32 argc, char **argv)
{
    char *input_data = read_entire_file("day3.input");

    stretchy_buf(Vec2) *first_line = 0;
    stretchy_buf(Vec2) *second_line = 0;
    stretchy_buf(StepPoint) *step_points = 0;

    buf_push(first_line, v2(0, 0));
    buf_push(second_line, v2(0, 0));

    StepPoint sp = {v2(0, 0), 0};
    buf_push(step_points, sp);

    s32 count = 0;
    Iterator line_iterator = {0, '\n', input_data};
    for (char *line = advance(&line_iterator);
         *line;
         line = advance(&line_iterator))
    {
        char *l = xmalloc(strlen(line) + 1);
        strcpy(l, line);

        s32 steps = 0;

        Iterator comma_iterator = {0, ',', l};
        for (char *instr = advance(&comma_iterator);
             *instr;
             instr = advance(&comma_iterator))
        {
            stretchy_buf(Vec2) *current_line = count == 0 ? first_line : second_line;

            s32 last_index = buf_len(current_line) - 1;
            Vec2 next_pos = current_line[last_index];

            char dir = instr[0];
            s32 val = atoi(instr + 1);
            steps += val;

            switch (dir)
            {
                case 'R':
                {
                    next_pos.x += val;
                } break;

                case 'L':
                {
                    next_pos.x -= val;
                } break;

                case 'U':
                {
                    next_pos.y += val;
                } break;

                case 'D':
                {
                    next_pos.y -= val;
                } break;
                
                default:
                {
                    assert(!"Invalid Path");
                } break;
            }

            if (current_line == first_line)
            {
                buf_push(first_line, next_pos);
            } else {
                buf_push(second_line, next_pos);
            }

            if (get_step_point_index(step_points, next_pos) == -1)
            {
                StepPoint sp = {next_pos, steps};
                buf_push(step_points, sp);
            }
        }

        ++count;
        steps = 0;
        free(l);
    }

    stretchy_buf(Vec2) *intersections = 0;

    for (s32 second_line_index = 0;
         second_line_index < buf_len(second_line) - 1;
         ++second_line_index)
    {
        Vec2 second_line_point = second_line[second_line_index];
        Vec2f p1 = v2_to_v2f(second_line_point);
        Vec2f p2 = v2_to_v2f(second_line[second_line_index + 1]);

        for (s32 first_line_index = 0;
             first_line_index < buf_len(first_line) - 1;
             ++first_line_index)
        {
            Vec2 first_line_point = first_line[first_line_index];
            Vec2f p3 = v2_to_v2f(first_line_point);
            Vec2f p4 = v2_to_v2f(first_line[first_line_index + 1]);

            f32 s1_x = p2.x - p1.x;
            f32 s1_y = p2.y - p1.y;
            f32 s2_x = p4.x - p3.x;
            f32 s2_y = p4.y - p3.y;

            f32 s, t;
            s = (-s1_y * (p1.x - p3.x) + s1_x * (p1.y - p3.y)) / (-s2_x * s1_y + s1_x * s2_y);
            t = ( s2_x * (p1.y - p3.y) - s2_y * (p1.x - p3.x)) / (-s2_x * s1_y + s1_x * s2_y);

            if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            {
                Vec2 point = v2(p1.x + (t * s1_x), p1.y + (t * s1_y));
                if (point.x == 0 && point.y == 0)
                {
                    continue;
                }

                buf_push(intersections, point);

                if (get_step_point_index(step_points, point) == -1)
                {
                    s32 first_line_index = get_step_point_index(step_points, first_line_point);
                    s32 second_line_index = get_step_point_index(step_points, second_line_point);
                    assert(first_line_index != -1);
                    assert(second_line_index != -1);

                    s32 steps = step_points[first_line_index].steps + step_points[second_line_index].steps;
                    steps += manhattan_distance(point, first_line_point);
                    steps += manhattan_distance(point, second_line_point);

                    StepPoint sp = {point, steps};
                    buf_push(step_points, sp);
                }
            }
        }
    }

    s32 num_step_points = buf_len(step_points);
    s32 num_intersections = buf_len(intersections);
    s32 distance = INT32_MAX;
    s32 min_steps = INT32_MAX;

    Vec2 origin = {0, 0};
    for (s32 inter_index = 0;
         inter_index < num_intersections;
         ++inter_index)
    {
        Vec2 p = intersections[inter_index];
        if (p.x == 0 && p.y == 0) continue;

        s32 dist = manhattan_distance(origin, p);
        if (dist < distance)
        {
            distance = dist;
        }

        s32 step_index = get_step_point_index(step_points, p);
        assert(step_index != -1);
        s32 steps = step_points[step_index].steps;
        if (steps < min_steps)
        {
            min_steps = steps;
        }
    }

    printf("Distance: %d\n", distance);
    printf("Steps: %d\n", min_steps);

    return 0;
}
