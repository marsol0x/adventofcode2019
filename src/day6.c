/* ==================================================================================
   $File: $
   $Date: 28 March 2020 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <string.h>

#include "common.h"

typedef struct OrbitMapping
{
    String name;
    struct OrbitMapping *orbits;
} OrbitMapping;

OrbitMapping * new_orbit_mapping(Map *orbit_map, String object_name)
{
    OrbitMapping *result = xmalloc(sizeof(OrbitMapping));
    result->name = object_name;
    result->orbits = 0;

    return result;
}

OrbitMapping * get_orbit_mapping(Map *orbit_map, String object_name)
{
    u64 key = hash_string(object_name);
    OrbitMapping *result = map_get(orbit_map, key);

    return result;
}

s32 main(s32 argc, char **argv)
{
    Map orbit_map = {0};
    char *input_data = read_entire_file("day6.input");

    Iterator iter = {0, '\n', input_data};
    for (char *line = advance(&iter);
         *line;
         line = advance(&iter))
    {
        // NOTE(marshel): Object)Satellite
        char *paren_pos = strchr(line, ')');
        *paren_pos = 0;

        String object_name = string(line);
        String satellite_name = string(paren_pos + 1);

        OrbitMapping *object_mapping = get_orbit_mapping(&orbit_map, object_name);
        if (!object_mapping)
        {
            object_mapping = new_orbit_mapping(&orbit_map, object_name);
            map_put_u64(&orbit_map, hash_string(object_name), object_mapping);
        }

        OrbitMapping *satellite_mapping = get_orbit_mapping(&orbit_map, satellite_name);
        if (!satellite_mapping)
        {
            satellite_mapping = new_orbit_mapping(&orbit_map, satellite_name);
            map_put_u64(&orbit_map, hash_string(satellite_name), satellite_mapping);
        }

        satellite_mapping->orbits = object_mapping;
    }

    OrbitMapping *santa = map_get(&orbit_map, hash_cstr("SAN"));
    OrbitMapping *you = map_get(&orbit_map, hash_cstr("YOU"));
    OrbitMapping **santa_map = 0; // NOTE(marshel): Stretchy buf

    for (OrbitMapping *om = santa->orbits;
         om;
         om = om->orbits)
    {
        buf_push(santa_map, om);
    }

    b32 found = false;
    s32 count = 0;
    for (OrbitMapping *om = you->orbits;
         om && !found;
         om = om->orbits)
    {
        for (s32 santa_map_index = 0;
             santa_map_index < buf_len(santa_map);
             ++santa_map_index)
        {
            if (om == santa_map[santa_map_index])
            {
                count += santa_map_index;
                found = true;
                break;
            }
        }

        if (!found) ++count;
    }

    printf("Orbital Transfers: %d\n", count);

    return 0;
}
