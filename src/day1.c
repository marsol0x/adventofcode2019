/* ==================================================================================
   $File: $
   $Date: 02 December 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>

#include "common.h"

s32 main(s32 argc, char **argv)
{
    char *input_file_path = "day1.input";
    char *file_contents = read_entire_file(input_file_path);

    s32 total_fuel = 0;
    Iterator iter = {0, '\n', file_contents};
    for (char *line = advance(&iter);
         *line;
         line = advance(&iter))
    {
        s32 mass = atoi(line);
        s32 fuel = (mass / 3) - 2;
        total_fuel += fuel;

        s32 fuel_mass = fuel;
        while (fuel_mass > 0)
        {
            s32 fuel = (fuel_mass / 3) - 2;
            if (fuel > 0)
            {
                total_fuel += fuel;
            }
            fuel_mass = fuel;
        }
    }
    printf("Total Fuel: %d\n", total_fuel);

    return 0;
}
