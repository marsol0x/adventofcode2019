@echo off

if not exist ..\build mkdir ..\build
pushd ..\build
    cl /nologo /Od /MTd /Zi /FC /Fd"adventofcode.pdb" /Fe"adventofcode.exe" ..\src\day7.c
popd
