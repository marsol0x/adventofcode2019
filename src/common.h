#ifndef COMMON_H
/* ==================================================================================
   $File: $
   $Date: 02 December 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// TODO(marshel): This didn't seem necessary in Windows
#include <stddef.h>
#include <stdint.h>

typedef long long s64;
typedef int       s32;
typedef short     s16;
typedef char      s8;

typedef unsigned long long u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char      u8;

typedef float  f32;
typedef double f64;

#define true (1==1)
#define false !true
typedef int b32;

#define MIN(a, b) ((a) > (b) ? (b) : (a))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define CLAMP_MAX(a, max) MIN(a, max)
#define CLAMP_MIN(a, min) MAX(a, min)

//
// NOTE(marshel): Linked List
//
#define DLIST_INIT(n) \
    (n)->prev = (n); \
    (n)->next = (n)

#define DLIST_ADD(l, n) \
    (n)->next = (l); \
    (n)->prev = (l)->prev; \
    (l)->prev->next = (n); \
    (l)->prev = (n)

#define DLIST_REMOVE(n) \
    (n)->next->prev = (n)->prev; \
    (n)->prev->next = (n)->next

//
// NOTE(marshel): Memory allocation stuff for debugging purposes
//

void * xmalloc(size_t size)
{
    void *result = malloc(size);

    return result;
}

void * xcalloc(size_t num, size_t size)
{
    void *result = calloc(num, size);

    return result;
}

void * xrealloc(void *ptr, size_t new_size)
{
    void *result = realloc(ptr, new_size);

    return result;
}

//
// NOTE(marshel): Files
//

char * read_entire_file(char *file_path)
{
    char *result = 0;
    s32 file_size = 0;
    FILE *input_file = fopen(file_path, "rb");

    fseek(input_file, 0, SEEK_END);
    file_size = ftell(input_file);
    fseek(input_file, 0, SEEK_SET);

    result = (char *)xmalloc(file_size + 1);
    fread(result, file_size, 1, input_file);
    result[file_size] = 0;
    fclose(input_file);

    return result;
}

//
// NOTE(marshel): Iterator
//

typedef struct Iterator
{
    s32 pos;
    char delimiter;
    char *data;
} Iterator;

// NOTE(marshel): This is destructive, if you want to maintain the original
// data, you need to make a copy of it.
char * advance(Iterator *iter)
{
    char *start = iter->data + iter->pos;
    char *end = start;

    while(*end && *end != iter->delimiter)
    {
        ++end;
    }

    iter->pos += end - start;
    if (*end)
    {
        ++iter->pos;
    }
    *end = 0;

    return start;
}

//
// NOTE(marshel): Stretchy Buffers
//

#define stretchy_buf(t) t

typedef struct BufferHeader
{
    size_t len;
    size_t cap;
    size_t elem_size;
    char buf[];
} BufferHeader;

#define buf__header(b) ((BufferHeader *)((char *)(b) - offsetof(BufferHeader, buf)))

#define buf_len(b) ((b) ? buf__header(b)->len : 0)
#define buf_cap(b) ((b) ? buf__header(b)->cap : 0)
#define buf_fit(b, n) ((n) <= buf_cap(b) ? 0 : ((b) = buf__grow((b), (n), sizeof(*(b)))))
#define buf_push(b, ...) (buf_fit((b), 1 + buf_len(b)), (b)[buf__header(b)->len++] = (__VA_ARGS__))
#define buf_free(b) ((b) ? (free(buf__header(b)), (b) = 0) : 0)
#define buf_clear(b) ((b) ? buf__header(b)->len = 0 : 0)

void * buf__grow(void *buf, size_t new_len, size_t elem_size)
{
    size_t new_cap = CLAMP_MIN(buf_cap(buf) + (buf_cap(buf) / 2), MAX(new_len, 16));
    size_t new_size = offsetof(BufferHeader, buf) + (new_cap * elem_size);

    BufferHeader *new_header;
    if (buf)
    {
        new_header = xrealloc(buf__header(buf), new_size);
        assert(new_header);
    } else {
        new_header = xmalloc(new_size);
        new_header->len = 0;
        new_header->elem_size = elem_size;
    }
    new_header->cap = new_cap;

    return new_header->buf;
}

void buf_remove(void *buf, s32 index)
{
    if (buf && buf_len(buf) > index)
    {
        char *src = (char *)buf + ((index + 1) * buf__header(buf)->elem_size);
        char *dest = (char *)buf + (index * buf__header(buf)->elem_size);
        size_t count = buf__header(buf)->elem_size * (buf_len(buf) - index);
        memcpy(dest, src, count);
        buf__header(buf)->len--;
    }
}

void * buf_copy(void *src)
{
    BufferHeader *result = 0;
    BufferHeader *src_header = buf__header(src);
    size_t size = sizeof(BufferHeader) + (src_header->cap * src_header->elem_size);
    result = xmalloc(size);

    result->len = src_header->len;
    result->cap = src_header->cap;
    result->elem_size = src_header->elem_size;

    memcpy(result->buf, src, src_header->len * src_header->elem_size);

    return result->buf;
}

//
// NOTE(marshel): Vectors
//

typedef struct Vec2
{
    s32 x, y;
} Vec2;

typedef struct Vec2f
{
    f32 x, y;
} Vec2f;

Vec2 v2(s32 x, s32 y)
{
    Vec2 result = {x, y};
    return result;
}

Vec2f v2f(f32 x, f32 y)
{
    Vec2f result = {x, y};
    return result;
}

Vec2f v2_to_v2f(Vec2 v)
{
    return v2f((f32)v.x, (f32)v.y);
}

b32 v2_eq(Vec2 a, Vec2 b)
{
    return a.x == b.x && a.y == b.y;
}

b32 v2f_eq(Vec2f a, Vec2f b)
{
    return a.x == b.x && a.y == b.y;
}

s32 manhattan_distance(Vec2 a, Vec2 b)
{
    s32 result = 0;

    result = abs(a.x - b.x) + abs(a.y - b.y);

    return result;
}

//
// NOTE(marshel): Hash Map
//

u64 hash_u64(u64 val)
{
    u64 h = val;

    h ^= h >> 33;
    h *= 0xff51afd7ed558ccdull;
    h ^= h >> 33;
    h *= 0xc4ceb9fe1a85ec53ull;
    h ^= h >> 33;

    return h;
}

u64 hash_bytes(const void *ptr, size_t len)
{
    // NOTE(marshel) FNV1a
    u64 result = 0;
    u64 fnv_bias = 0xcbf29ce484222325ull;
    u64 fnv_prime = 0x100000001b3ull;
    const char *buf = (const char *)ptr;

    result = fnv_bias;
    for (size_t index = 0;
         index < len;
         ++index)
    {
        result ^= buf[index];
        result *= fnv_prime;
    }
    
    return result;
}

typedef struct Map
{
    u64 *keys;
    u64 *vals;
    size_t cap;
    size_t len;
} Map;

void map_put_u64_to_u64(Map *map, u64 key, u64 val);

void map_grow(Map *map, size_t new_cap)
{
    new_cap = CLAMP_MIN(new_cap, 16);
    Map new_map = {
        .keys = xcalloc(new_cap, sizeof(u64)),
        .vals = xcalloc(new_cap, sizeof(u64)),
        .cap = new_cap,
        .len = 0,
    };

    for (size_t i = 0;
         i < map->cap;
         ++i)
    {
        if (map->keys[i])
        {
            map_put_u64_to_u64(&new_map, map->keys[i], map->vals[i]);
        }
    }

    free(map->keys);
    free(map->vals);
    *map = new_map;
}

void map_put_u64_to_u64(Map *map, u64 key, u64 val)
{
    assert(key);
    if (!val)
    {
        return;
    }

    if (2*map->len >= map->cap)
    {
        map_grow(map, 2*map->cap);
    }

    u64 hash = hash_u64(key);
    u64 index = hash % map->cap;
    for (;;)
    {
        if (!map->keys[index])
        {
            map->keys[index] = key;
            map->vals[index] = val;
            ++map->len;
            return;
        } else if (map->keys[index] == key) {
            map->vals[index] = val;
            return;
        }

        ++index;
        if (index == map->cap)
        {
            index = 0;
        }
    }
}

void map_put_u64(Map *map, u64 key, void *val)
{
    map_put_u64_to_u64(map, key, (u64)((uintptr_t)val));
}

void map_put(Map *map, void *key, void *val)
{
    map_put_u64_to_u64(map, (u64)(uintptr_t)key, (u64)(uintptr_t)val);
}

u64 map_get_u64(Map *map, u64 key)
{
    if (map->len == 0)
    {
        return 0;
    }

    u64 hash = hash_u64(key);
    u64 index = hash % map->cap;
    for (;;)
    {
        if (map->keys[index] == key)
        {
            return map->vals[index];
        } else if (!map->keys[index]) {
            return 0;
        }

        ++index;
        if (index == map->cap)
        {
            index = 0;
        }
    }
}

void * map_get(Map *map, u64 key)
{
    void *val = (void *)(uintptr_t)map_get_u64(map, key);
    return val;
}

//
// NOTE(marshel): Strings
//

typedef struct String
{
    size_t len;
    char *str;
} String;

#define hash_string(s) hash_bytes((s).str, (s).len)
#define hash_cstr(s) hash_bytes((s), strlen((s)))

String string(char *c_string) // TODO(marshel): LEAK, user is responsible for freeing strings
{
    String result;
    size_t len = strlen(c_string);

    result.str = xmalloc(len + 1);
    memcpy(result.str, c_string, len);
    result.len = len;
    result.str[len] = 0;

    return result;
}

#define COMMON_H
#endif

