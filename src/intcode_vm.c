/* ==================================================================================
   $File: $
   $Date: 7 April 2020 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include "common.h"

typedef enum OpCode
{
    ADD           = 1,
    MULTIPLY      = 2,
    INPUT         = 3,
    OUTPUT        = 4,
    JUMP_IF_TRUE  = 5,
    JUMP_IF_FALSE = 6,
    LESS_THAN     = 7,
    EQUALS        = 8,

    END           = 99,
} OpCode;

typedef enum ParameterMode
{
    POSITION_MODE,
    IMMEDIATE_MODE,
} ParameterMode;

typedef struct Instruction
{
    s32 opcode;
    ParameterMode modes[3];
} Instruction;

Instruction instruction(s32 intcode)
{
    Instruction result = {0};

    result.opcode = intcode % 100;
    result.modes[0] = (intcode / 100) % 10;
    result.modes[1] = (intcode % 10000) / 1000;
    result.modes[2] = (intcode % 100000) / 10000;

    return result;
}

s32 get_value(s32 *intcode, ParameterMode mode, s32 param_pos)
{
    s32 result = 0;
    s32 param = intcode[param_pos];
    switch (mode)
    {
        case POSITION_MODE:
        {
            result = intcode[param];
        } break;

        case IMMEDIATE_MODE:
        {
            result = param;
        } break;

        default:
        {
            assert(!"Invalid path");
        } break;
    }

    return result;
}

void set_value(s32 *intcode, ParameterMode mode, s32 param_pos, s32 value)
{
    s32 param = intcode[param_pos];
    switch (mode)
    {
        case POSITION_MODE:
        {
            intcode[param] = value;
        } break;

        case IMMEDIATE_MODE:
        {
            // NOTE(marshel): I don't know if this is correct, it's not
            // particularly clear.
            assert(!"Unable to store immediate mode values.");
        } break;

        default:
        {
            assert(!"Invalid path");
        } break;
    }
}

s32 run_code(s32 *intcode_data, s32 *input_values, s32 input_values_count)
{
    s32 *intcode = buf_copy(intcode_data);

    s32 output_value = 0;
    s32 intcode_pos = 0;
    s32 input_value_pos = 0;
    b32 running = true;
    while (running)
    {
        Instruction inst = instruction(intcode[intcode_pos++]);
        switch (inst.opcode)
        {
            case ADD:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);
                s32 b = get_value(intcode, inst.modes[1], intcode_pos++);
                s32 result = a + b;

                set_value(intcode, inst.modes[2], intcode_pos++, result);
            } break;

            case MULTIPLY:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);
                s32 b = get_value(intcode, inst.modes[1], intcode_pos++);
                s32 result = a * b;

                set_value(intcode, inst.modes[2], intcode_pos++, result);
            } break;

            case INPUT:
            {
                assert(input_value_pos < input_values_count);

                s32 input_value = input_values[input_value_pos++];
                set_value(intcode, inst.modes[0], intcode_pos++, input_value);
            } break;

            case OUTPUT:
            {
                s32 value = get_value(intcode, inst.modes[0], intcode_pos++);
                output_value = value;
            } break;

            case JUMP_IF_TRUE:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);

                if (a)
                {
                    intcode_pos = get_value(intcode, inst.modes[1], intcode_pos);
                } else {
                    ++intcode_pos;
                }
            } break;

            case JUMP_IF_FALSE:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);

                if (!a)
                {
                    intcode_pos = get_value(intcode, inst.modes[1], intcode_pos);
                } else {
                    ++intcode_pos;
                }
            } break;

            case LESS_THAN:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);
                s32 b = get_value(intcode, inst.modes[1], intcode_pos++);
                s32 result = a < b;

                set_value(intcode, inst.modes[2], intcode_pos++, result);
            } break;

            case EQUALS:
            {
                s32 a = get_value(intcode, inst.modes[0], intcode_pos++);
                s32 b = get_value(intcode, inst.modes[1], intcode_pos++);
                s32 result = a == b;

                set_value(intcode, inst.modes[2], intcode_pos++, result);
            } break;

            case END:
            {
                running = false;
            } break;

            default:
            {
                assert(!"Invalid OpCode");
            } break;
        }
    }

    buf_free(intcode);
    return output_value;
}
